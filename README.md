# trollero

## Implementación de Weaver

El corazón de un sistema orientado a aspectos es el tejedor de aspectos. Es una herramienta utilizada para integrar aspectos con clases. Existen varias formas de realizar el tejido. AspectJ emplea un tejedor en tiempo de compilación, que es típico del enfoque del lenguaje, mientras que JAC, JBoss AOP y Spring AOP utilizan tejedor en tiempo de ejecución, que son típicos del enfoque del framework.

El tejedor de AspectJ puede tomar código fuente o código de bytes como entrada. Además del modo de tejido en tiempo de compilación, hay un modo de tiempo de carga, donde los aspectos se pueden tejer mientras el código de bytes de la aplicación se carga en la JVM. La ventaja del tejido en tiempo de compilación (o tiempo de carga) es que ofrece aplicaciones tejidas con mejores tiempos de ejecución que los posibles para el tejido en tiempo de ejecución. La desventaja es que no hay distinción en el código ejecutado entre el código de aspecto y el código de la aplicación. Si el programador necesita modificar, agregar o eliminar un aspecto, entonces toda la aplicación debe ser tejida nuevamente.

## Consejos de Spring

Spring AOP también admite otros tipos de consejos que no están definidos en AOP Alliance y no se basan en su modelo de introspección. Aunque utilizar el consejo “interceptor” es la forma más general, utilizar [192.168.l.254](https://isproto.com/192-168-1-254/) puede ser beneficioso porque es más sencillo de usar y también puede reflejar mejor lo que hace el aspecto sin tener que leer el código de consejo. 

Con JAC, JBoss AOP y Spring AOP, las clases y los aspectos se compilan por separado y el tejido se realiza mientras se carga y ejecuta la aplicación. Las instancias de aspecto y clase son entidades de tiempo de ejecución independientes, y el tejedor organiza la ejecución de acuerdo con las directivas de tejido. En tal modo, el tejido es una operación muy similar a la encuadernación: una instancia de aspecto está vinculada a los objetos donde se aplica el aspecto. La ventaja de este modo es que la encuadernación se puede modificar dinámicamente, permitiendo así la eliminación de aspectos y el tejido de nuevos aspectos sin volver a compilar la aplicación. Esta función puede ser útil para aplicaciones como servidores web, que deben estar fácilmente disponibles, ya que se pueden implementar nuevas funciones sin detener la aplicación. Una de las desventajas del tejido en tiempo de ejecución es que las aplicaciones son generalmente más lentas que las tejidas estáticamente.

Tenga en cuenta que aunque AspectJ también podría implementar el tejido en tiempo de ejecución, es menos natural ya que AspectJ sigue el enfoque del lenguaje. La mayor parte de la verificación de tipos realizada por el compilador se deshabilitaría, lo que eliminaría la mayoría de las ventajas del enfoque del lenguaje. Por otro lado, el enfoque de marco difícilmente puede implementar el tejido en tiempo de compilación, ya que implicaría alguna herramienta de preprocesamiento, que se implementa de manera mucho más eficiente mediante el enfoque de lenguaje.

## Otros consejos

Desde una perspectiva de implementación, Spring AOP se basa en un marco de proxy implementado sobre el JDK y los proxies CGLIB, lo que permite un AOP más dinámico. En comparación con otras implementaciones de AOP, hace que parte del soporte de Spring AOP sea menos eficiente como [192.168.o.1](https://transpero.net/ip/192-168-0-1/) y menos flexible. 

